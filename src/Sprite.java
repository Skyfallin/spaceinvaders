import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

/**
 * Creates a sprite, inherits from the Polygon class. This represents any entity on the root pane.
 */
public class Sprite extends Polygon {

    // instance variables responsible for controlling whether the sprite is dead, its type
    // "player, enemy, etc"
    private boolean dead = false;
    private String type;

    Sprite(int x, int y, String type, Color fill) {
        super();
        this.setFill(fill);
        this.setLayoutX(x);
        this.setLayoutY(y);
        this.type = type;
    }

    // methods to move the sprite across the screen
    public void moveLeft() {
        setTranslateX(getTranslateX() - 5);
    }

    public void moveRight() {
        setTranslateX(getTranslateX() + 5);
    }

    public void moveUp() {
        setTranslateY(getTranslateY() - 5);
    }

    public void moveDown() {
        setTranslateY(getTranslateY() + 5);
    }

    public void moveLeft(int amt) {
        setTranslateX(getTranslateX() - amt);
    }

    public void moveRight(int amt) {
        setTranslateX(getTranslateX() + amt);
    }

    public void moveUp(int amt) {
        setTranslateY(getTranslateY() - amt);
    }

    public void moveDown(int amt) {
        setTranslateY(getTranslateY() + amt);
    }

    public boolean getDead() {
        return this.dead;
    }

    public void setDead() {
        this.dead = true;
    }

    public String getType() {
        return this.type;
    }
}