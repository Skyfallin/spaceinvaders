import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("FieldCanBeLocal")
public class SpaceInvadersApp extends Application {

    private Pane root = new Pane();
    private Label levelLabel, scoreLabel, w, a, s, d;
    private final int WINDOW_WIDTH = 700;
    private final int WINDOW_HEIGHT = 800;
    private Sprite player, heart1, heart2, upArr, leftArr, downArr, rightArr;
    private int playerLives = 2;
    private int playerScore = 0;
    private double t = 0; // used for AnimationTimer
    private int level = 1;
    private int stage = 0; // stage variable to control # of enemies
    private double shootChance = 0.3; // chance an enemy will fire a missile. Incremented each level.

    /**
     * Equivalent of driver code.
     * Runs the scene, controls player movement.
     * Player is prevented from flying 'offscreen'
     */
    @Override
    public void start(Stage stage) {

        Scene scene = new Scene(createContent());

        scene.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case W:
                    if (player.getLayoutY() + player.getTranslateY() <= 9) return;
                    player.moveUp(9);
                    break;
                case A:
                    if (player.getLayoutX() + player.getTranslateX() <= 9) return;
                    player.moveLeft(9);
                    break;
                case S:
                    if (player.getLayoutY() + player.getTranslateY() > this.WINDOW_HEIGHT - 30) return;
                    player.moveDown(9);
                    break;
                case D:

                    if (player.getLayoutX() + player.getTranslateX() > this.WINDOW_WIDTH - 40) return;
                    player.moveRight(9);
                    break;
                case SPACE:
                    shoot(player);
                    break;
            }
        });

        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("CSE205 - Space Invaders");
        stage.show();
    }

    /**
     * Create parent to initialize instance variables
     */
    private Parent createContent() {

        // setup level label
        levelLabel = new Label("Level " + level);
        levelLabel.setLayoutX(225);
        levelLabel.setLayoutY(20);
        levelLabel.setFont(new Font("Arial", 30));
        levelLabel.setTextFill(Color.WHEAT);

        // setup score label
        scoreLabel = new Label("Score " + playerScore);
        scoreLabel.setLayoutX(375);
        scoreLabel.setLayoutY(20);
        scoreLabel.setFont(new Font("Arial", 30));
        scoreLabel.setTextFill(Color.LIGHTGREEN);

        // setup life display
        heart1 = new Sprite(595, 645, "heart", Color.RED);
        heart1.getPoints().addAll(0d, 5d, 5d, 0d, 10d, 5d, 15d, 6d, 20d, 5d, 25d, 0d, 30d, 5d, 30d, 10d, 15d, 25d, 0d, 10d);
        heart2 = new Sprite(595+50, 645, "heart", Color.RED);
        heart2.getPoints().addAll(0d, 5d, 5d, 0d, 10d, 5d, 15d, 6d, 20d, 5d, 25d, 0d, 30d, 5d, 30d, 10d, 15d, 25d, 0d, 10d);


        // setup controls labels
        w = new Label("W");
        w.setLayoutX(625);
        w.setLayoutY(696);
        w.setFont(new Font("Arial", 20));
        w.setTextFill(Color.WHITE);
        w.setOpacity(0.75);
        a = new Label("A");
        a.setLayoutX(581);
        a.setLayoutY(742);
        a.setFont(new Font("Arial", 20));
        a.setTextFill(Color.WHITE);
        a.setOpacity(0.75);
        s = new Label("S");
        s.setLayoutX(628);
        s.setLayoutY(787);
        s.setFont(new Font("Arial", 20));
        s.setTextFill(Color.WHITE);
        s.setOpacity(0.75);
        d = new Label("D");
        d.setLayoutX(676);
        d.setLayoutY(741);
        d.setFont(new Font("Arial", 20));
        d.setTextFill(Color.WHITE);
        d.setOpacity(0.75);

        // create controls display
        upArr = new Sprite(625, 718, "gui", Color.WHITE);
        upArr.getPoints().addAll(0d, 10d, 10d, 0d, 20d, 10d, 15d, 10d, 15d, 25d, 5d, 25d, 5d, 10d);
        upArr.setOpacity(0.5);
        leftArr = new Sprite(596, 743, "gui", Color.WHITE);
        leftArr.getPoints().addAll(0d, 10d, 10d, 0d, 10d, 5d, 25d, 5d, 25d, 15d, 10d, 15d, 10d, 20d);
        leftArr.setOpacity(0.5);
        downArr = new Sprite(625, 763, "gui", Color.WHITE);
        downArr.getPoints().addAll(0d, 15d, 10d, 25d, 20d, 15d, 15d, 15d, 15d, 0d, 5d, 0d, 5d, 15d);
        downArr.setOpacity(0.5);
        rightArr = new Sprite(650, 743, "gui", Color.WHITE);
        rightArr.getPoints().addAll(0d, 5d, 15d, 5d, 15d, 0d, 25d, 10d, 15d, 20d, 15d,15d, 0d,15d);
        rightArr.setOpacity(0.5);

        // create player sprite - collection of coordinate points (x, y)
        player = new Sprite(325, 700, "player", Color.YELLOW);
        player.getPoints().addAll(0d, 30D, 20D, 0d, 40d, 30D, 20D, 19d);

        // add elements to root
        root.setPrefSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        root.getChildren().addAll(levelLabel, scoreLabel, player, heart1, heart2, upArr, leftArr, downArr, rightArr, w, a, s, d);

        // begin animation timer
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
            }
        };
        timer.start();

        // setup level
        nextLevel();

        root.setBackground(new Background(new BackgroundFill(Color.rgb(40, 40, 40), CornerRadii.EMPTY, Insets.EMPTY)));
        return root;
    }

    /**
     * Handles logic for level progression.
     * Bosses appear on levels 8, 16, and 24, 25 is final level.
     */
    private void nextLevel() {
        stage++;
        if (level == 8) {
            levelLabel.setText("Boss");
            Sprite s = new Sprite(295, 150, "boss", Color.WHITE);
            s.getPoints().addAll(0d, 0d, 60d, 80d, 120d, 0d);
            root.getChildren().add(s);
            stage = 0;
        } else if (level == 16) {
            levelLabel.setText("Boss");
            Sprite s = new Sprite(180, 150, "boss", Color.WHITE);
            s.getPoints().addAll(0d, 0d, 60d, 80d, 120d, 0d);
            Sprite s2 = new Sprite(420, 150, "boss", Color.WHITE);
            s2.getPoints().addAll(0d, 0d, 60d, 80d, 120d, 0d);
            root.getChildren().addAll(s, s2);
            stage = 0;
        } else if (level == 24) {
            levelLabel.setText("Boss");
            Sprite s = new Sprite(90, 150, "boss", Color.WHITE);
            s.getPoints().addAll(0d, 0d, 60d, 80d, 120d, 0d);
            Sprite s2 = new Sprite(290, 150, "boss", Color.WHITE);
            s2.getPoints().addAll(0d, 0d, 60d, 80d, 120d, 0d);
            Sprite s3 = new Sprite(490, 150, "boss", Color.WHITE);
            s3.getPoints().addAll(0d, 0d, 60d, 80d, 120d, 0d);
            root.getChildren().addAll(s, s2, s3);
        } else if (level == 25) {

            root.getChildren().remove(scoreLabel);
            levelLabel.setText("WINNER - (OG IS CAKU)");
            levelLabel.setTextFill(Color.LIGHTGREEN);
            levelLabel.setLayoutX(285);

            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            Platform.exit();
                        }
                    },
                    5000
            );

        } else {
            for (int i = 0; i < stage; i++) {
                Sprite s = new Sprite(50 + i * 100, 150, "enemy", Color.WHITE);
                s.getPoints().addAll(0d, 0d, 15d, 20d, 30d, 0d);
                root.getChildren().add(s);
            }
        }
    }

    /**
     * Grabs a filtered list of Sprites from root
     */
    private List<Sprite> sprites() {
        return root.getChildren().stream().filter(component -> component instanceof Sprite).map(n -> (Sprite) n).collect(Collectors.toList());
    }

    /**
     * Moves bullets, handles player death, etc.
     */
    private void update() {

        t += 0.01;

        sprites().forEach(s -> {
            switch (s.getType()) {
                case "bossbullet":
                    s.moveDown(13);

                    // players dies if hit by enemy bullet
                    // close program if player dies
                    if (s.getBoundsInParent().intersects(player.getBoundsInParent())) {

                        // player gets multiple lives
                        if (playerLives > 1) {
                            removeLifeResetScore(s);
                            return;
                        } else {
                            killPlayer(s);
                        }
                    }
                    break;
                case "enemybullet":
                    if (level <= 8) {
                        s.moveDown();
                    } else if (level <= 16) { // increase missile speed after first boss
                        s.moveDown(8);
                    } else {
                        s.moveDown(11); // increase missile speed after second boss
                    }

                    // players dies if hit by enemy bullet
                    // close program if player dies
                    if (s.getBoundsInParent().intersects(player.getBoundsInParent())) {
                        if (playerLives > 1) {
                            removeLifeResetScore(s);
                        } else {
                            killPlayer(s);
                        }
                    }
                    break;
                // enemy dies if hit by player bullet
                case "playerbullet":
                    s.moveUp();
                    sprites().stream().filter(e -> e.getType().equals("enemy") || e.getType().equals("boss")).forEach(enemy -> {
                        if (s.getBoundsInParent().intersects(enemy.getBoundsInParent())) {
                            enemy.setDead();
                            s.setDead();
                            playerScore++;
                            scoreLabel.setText("Score " + playerScore);
                        }
                    });
                    break;
                case "enemy":
                    if (t > 2) {
                        if (Math.random() < shootChance) {
                            shoot(s);
                        }
                    }
                    break;
                case "boss":
                    if (t > 2) {
                        if (Math.random() < shootChance) {
                            shoot(s);
                        }
                    }
                    break;
            }
        });

        // remove missiles, ensure instanceof sprite
        root.getChildren().removeIf(n -> {
            if (!(n instanceof Sprite)) return false;
            Sprite s = (Sprite) n;
            return s.getDead();
        });

        // move the player to the next level if the enemies are cleared
        if (level >= 25) return;
        Predicate<Node> enemy = s -> ((Sprite) s).getType().equals("enemy") || ((Sprite) s).getType().equals("boss");
        if (root.getChildren().stream().filter(e -> e instanceof Sprite).noneMatch(enemy)) {
            level++;
            shootChance += 0.1;
            levelLabel.setText("Level " + level);
            nextLevel();
        }

        // reset counter
        if (t > 2) {
            t = 0;
        }
    }

    /**
     * If the player dies with 2 lives remaining, they lose a life and their score is reset.
     */
    private void removeLifeResetScore(Sprite s) {
        s.setDead();
        playerLives--;
        heart1.setDead();
        playerScore = 0;
        scoreLabel.setText("Score " + playerScore);
    }

    /**
     * If the player dies with 1 life remaining, the game terminates and the player sees "GAME OVER"
     */
    private void killPlayer(Sprite s) {
        heart2.setDead();
        player.setDead();
        s.setDead();

        root.getChildren().remove(scoreLabel);
        levelLabel.setText("GAME OVER");
        levelLabel.setTextFill(Color.ORANGERED);
        levelLabel.setLayoutX(265);
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        Platform.exit();
                    }
                },
                5000
        );
    }

    /**
     * Creates sprites to act as missiles, (x3 if boss level)
     */
    private void shoot(Sprite whoShot) {
        double missileWidth = 4d;
        int whoShotWidth = 20;
        if (whoShot.getType().equals("boss")) {
            missileWidth = 8d;
            whoShotWidth = 60;
        }
        if (whoShot.getType().equals("enemy")) {
            whoShotWidth = 15;
        }
        int locX = (int) (whoShot.getLayoutX() + whoShot.getTranslateX() + whoShotWidth - (missileWidth / 2));
        Color color;
        if (whoShot.getType().equals("player")) {
            color = Color.LIGHTYELLOW;
        } else {
            color = Color.PALEVIOLETRED;
        }
        Sprite s = createMissile(whoShot, missileWidth, locX, color, 0);
        root.getChildren().add(s);
        if (whoShot.getType().equals("boss")) {
            Sprite s2 = createMissile(whoShot, missileWidth, locX, color, 55);
            Sprite s3 = createMissile(whoShot, missileWidth, locX, color, -55);
            root.getChildren().addAll(s2, s3);
        }
    }

    /**
     * Used in the shoot method
     * @param whoShot - sprite which fired the missile
     * @param missileWidth - how wide the missile should be
     * @param locX - used to determine where the missile sprite appears on the screen
     * @param color - color of the missile
     * @param shift - used to offset the missiles if a sprite fires multiple.
     */
    private Sprite createMissile(Sprite whoShot, double missileWidth, int locX, Color color, int shift) {
        Sprite s = new Sprite(locX, (int) (whoShot.getLayoutY() + whoShot.getTranslateY()), whoShot.getType() + "bullet", color);
        s.getPoints().addAll(0d, 10d, missileWidth, 10d, missileWidth, 0d, 0d, 0d);
        if (shift != 0) {
            s.setTranslateX(shift);
        }
        return s;
    }
}
